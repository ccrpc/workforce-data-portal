# wdp-resource-popover



<!-- Auto Generated Below -->


## Properties

| Property    | Attribute | Description                                     | Type         | Default                                                                                                                                                                                                                                                                                                                                                                                                                                             |
| ----------- | --------- | ----------------------------------------------- | ------------ | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `resources` | --        | Resources object to construct the resource link | `Resource[]` | `[   {     title: 'County Profile',     description: 'Learn more about workforce characteristic for Champaign, Douglas,\     Ford, Iroquois, and Piatt counties.',     href: 'https://ccrpc.org/programs/workforce-development/employer-and-business-services/county-profile/'   },   {     title: 'East Central Illinois Worknet',     description: 'Information for both job seekers and employers.',     href: 'https://eciwork.net/'   },    ]` |


## Dependencies

### Used by

 - [wdp-resource](../wdp-resource)

### Depends on

- ion-list
- ion-list-header
- ion-label
- ion-item

### Graph
```mermaid
graph TD;
  wdp-resource-popover --> ion-list
  wdp-resource-popover --> ion-list-header
  wdp-resource-popover --> ion-label
  wdp-resource-popover --> ion-item
  ion-item --> ion-icon
  ion-item --> ion-ripple-effect
  wdp-resource --> wdp-resource-popover
  style wdp-resource-popover fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
