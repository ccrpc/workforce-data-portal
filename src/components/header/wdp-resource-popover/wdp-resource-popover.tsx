import { Component, Host, Prop, h } from '@stencil/core';

export interface Resource {
  title: string;
  description: string;
  href: string;
}

@Component({
  tag: 'wdp-resource-popover',
  styleUrl: 'wdp-resource-popover.css',
  shadow: true,
})
export class WdpResourcePopover {
  /**
   * Resources object to construct the resource link
   */
  @Prop() resources: Resource[] = [
  {
    title: 'County Profile',
    description: 'Learn more about workforce characteristic for Champaign, Douglas,\
    Ford, Iroquois, and Piatt counties.',
    href: 'https://ccrpc.org/programs/workforce-development/employer-and-business-services/county-profile/'
  },
  {
    title: 'East Central Illinois Worknet',
    description: 'Information for both job seekers and employers.',
    href: 'https://eciwork.net/'
  },
  
];
  render() {
    return (
      <Host>
        <slot>
        <ion-list>
        <ion-list-header>
          <h1><ion-label>Resources</ion-label></h1>
        </ion-list-header>
        {this.resources.map((r) => 
          <ion-item href={r.href}>
            <ion>
              <b>{r.title}</b> - {r.description}
            </ion>
          </ion-item>
        )}
        </ion-list>
        </slot>
      </Host>
    );
  }

}
