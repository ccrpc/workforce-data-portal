import { newSpecPage } from '@stencil/core/testing';
import { WdpResourcePopover } from '../wdp-resource-popover';

describe('wdp-resource-popover', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [WdpResourcePopover],
      html: `<wdp-resource-popover></wdp-resource-popover>`,
    });
    expect(page.root).toBeTruthy;
  });
});
