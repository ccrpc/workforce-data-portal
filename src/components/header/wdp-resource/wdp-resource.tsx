import { Component, Host, Prop, h } from '@stencil/core';

@Component({
  tag: 'wdp-resource',
  styleUrl: 'wdp-resource.css'
})
export class WdpResource {
  /**
   * label for the button
   */
  @Prop() label: string = 'Resources';
  /**
   * Popup controller for resources
   */
  @Prop({ connect: 'ion-popover-controller' })
	popoverCtrl!: HTMLIonPopoverControllerElement;

  async openPopover(_e) {
		const popover = await this.popoverCtrl.create({
			component: document.createElement('wdp-resource-popover')
		});
		await popover.present();
		return popover;
  }
  
  render() {
    return (
      <Host>
        <slot>
          <ion-button ion-button onClick={(e) => this.openPopover(e)}>
            {this.label}
          </ion-button>
        </slot>
      </Host>
    );
  }

}
