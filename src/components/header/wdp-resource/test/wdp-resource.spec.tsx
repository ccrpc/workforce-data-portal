import { newSpecPage } from '@stencil/core/testing';
import { WdpResource } from '../wdp-resource';

describe('wdp-resource', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [WdpResource],
      html: `<wdp-resource></wdp-resource>`,
    });
    expect(page.root).toBeTruthy;
  });
});
