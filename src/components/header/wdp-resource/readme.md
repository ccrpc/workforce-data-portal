# wdp-resource



<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description          | Type     | Default       |
| -------- | --------- | -------------------- | -------- | ------------- |
| `label`  | `label`   | label for the button | `string` | `'Resources'` |


## Dependencies

### Used by

 - [wdp-app](../../app)

### Depends on

- [wdp-resource-popover](../wdp-resource-popover)
- ion-button
- ion-popover-controller

### Graph
```mermaid
graph TD;
  wdp-resource --> wdp-resource-popover
  wdp-resource --> ion-button
  wdp-resource --> ion-popover-controller
  wdp-resource-popover --> ion-list
  wdp-resource-popover --> ion-list-header
  wdp-resource-popover --> ion-label
  wdp-resource-popover --> ion-item
  ion-item --> ion-icon
  ion-item --> ion-ripple-effect
  ion-button --> ion-ripple-effect
  wdp-app --> wdp-resource
  style wdp-resource fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
