# wdp-info-button

This is the info popup compoent that shows additional information.

<!-- Auto Generated Below -->


## Dependencies

### Used by

 - [wdp-app](../../app)

### Depends on

- [wdp-info](../infopopover)
- ion-button
- ion-icon
- ion-popover-controller

### Graph
```mermaid
graph TD;
  wdp-info-button --> wdp-info
  wdp-info-button --> ion-button
  wdp-info-button --> ion-icon
  wdp-info-button --> ion-popover-controller
  wdp-info --> ion-card
  wdp-info --> ion-card-header
  wdp-info --> ion-card-title
  wdp-info --> ion-card-content
  ion-card --> ion-ripple-effect
  ion-button --> ion-ripple-effect
  wdp-app --> wdp-info-button
  style wdp-info-button fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
