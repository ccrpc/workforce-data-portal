import { newSpecPage } from '@stencil/core/testing';
import { WdpInfoButton } from './info';

describe('Info', () => {
  it('should render the info button', async() => {
    const page = await newSpecPage({
      components: [WdpInfoButton],
      html: `<wdp-info></wdp-info>`,
    });
    expect(page.root).toBeTruthy;
  });
});
