import { h, Component, Prop } from '@stencil/core';

@Component({
	tag: 'wdp-info-button'
})
export class WdpInfoButton {
	@Prop({ connect: 'ion-popover-controller' })
	popoverCtrl!: HTMLIonPopoverControllerElement;

	async openPopover(_e) {
		const popover = await this.popoverCtrl.create({
			component: document.createElement('wdp-info')
		});
		await popover.present();
		return popover;
	}

	render() {
		return (
			<ion-button ion-button onClick={(e) => this.openPopover(e)}>
				<ion-icon name="information-circle-outline" />
			</ion-button>
		);
	}
}
