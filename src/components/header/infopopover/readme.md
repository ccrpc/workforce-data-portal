# wdp-info

This is the popover controller for the info icon.

<!-- Auto Generated Below -->


## Dependencies

### Used by

 - [wdp-info-button](../info)

### Depends on

- ion-card
- ion-card-header
- ion-card-title
- ion-card-content

### Graph
```mermaid
graph TD;
  wdp-info --> ion-card
  wdp-info --> ion-card-header
  wdp-info --> ion-card-title
  wdp-info --> ion-card-content
  ion-card --> ion-ripple-effect
  wdp-info-button --> wdp-info
  style wdp-info fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
