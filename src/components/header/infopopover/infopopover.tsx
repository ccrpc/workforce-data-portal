import { h, Component } from '@stencil/core';

@Component({
	tag: 'wdp-info'
})
export class WpdInfo {
	render() {
		let root = document.querySelector('wdp-app');
		return (
			<ion-card>
				<ion-card-header>
					<ion-card-title>{root.label}</ion-card-title>
				</ion-card-header>
				<ion-card-content>
					<p>
          This data portal contains workforce related data from
          <a href="https://www.census.gov/"> U.S. Census
          Bureau</a>, <a href="https://www.bls.gov/"> U.S. Bureau of Labor 
          Statistics</a>, and <a href="https://www2.illinois.gov/ides/Pages/default.aspx">
           Illinois Department of Employment Security</a> for LWIA 17 in the
           central Illinois region.
          </p>
          <br />
          <p>
          This portal is developed by the 
          <a href="https://ccrpc.org"> Champaign County Regional Planning
          Commission</a>. For more workforce related resources, visit the{' '}
          <a href="https://ccrpc.org/workforce">
            Workforce Development's website
          </a>.
					</p>
				</ion-card-content>
			</ion-card>
		);
	}
}
