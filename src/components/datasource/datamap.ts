export const countyMap = {
  'C': 'Champaign',
  'D': 'Douglas',
  'F': 'Ford',
  'I': 'Iroquois',
  'P': 'Piatt',
  'V': 'Vermilion'
};

export const industryMap = {
  '0': 'Agriculture, Forestry, Fishing and Hunting',
  '1': 'Mining',
  '2': 'Utilities',
  '3': 'Construction',
  '4': 'Manufacturing',
  '5': 'Wholesale Trade',
  '6': 'Retail Trade',
  '7': 'Transportation and Warehousing',
  '8': 'Information',
  '9': 'Finance and Insurance',
  'A': 'Real Estate Rental and Leasing',
  'B': 'Professional, Scientific, and Technical Services',
  'C': 'Management of Companies and Enterprises',
  'D': 'Administrative and Support and Waste Management and Remediation Services',
  'E': 'Educational Services',
  'F': 'Health Care and Social Assistance',
  'G': 'Arts, Entertainment, and Recreation',
  'H': 'Accommodation and Food Services',
  'I': 'Other Services (except Public Administration)',
  'J': 'Public Administration'
};

export const workerMap = {
  'M': 'Male',
  'F': 'Female',
  'W': 'White',
  'B': 'Black or African American Alone',
  'N': 'American Indian or Alaska Native Alone',
  'A': 'Asian Alone',
  'P': 'Native Hawaiian or Other Pacific Islander Alone',
  'T': 'Two or More Race Groups',
  'H': 'Hispanic',
  'K': 'Non-Hispanic'
};

export const businessMap = {
  'C': 'State and local government plus private ownership',
  'P': 'Private',
  '1': '0-19 Employees',
  '2': '20-49 Employees',
  '3': '50-249 Employees',
  '4': '250-499 Employees',
  '5': '500+ Employees'
};