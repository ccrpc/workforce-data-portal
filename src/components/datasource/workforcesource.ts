import * as d3Fetch from "d3-fetch";
import { DataSource } from '@ccrpc/charts';
import { countyMap, industryMap, workerMap, businessMap } from './datamap';


export class WorkforceSource extends DataSource {
  protected data: any;
  protected dataCache = {};
  protected cache: any = {};

  public getData(url: string, _sanitize: boolean = false) {
    /**
     * Download and cache the data
     */
    let baseUrl = url.split('?')[0];
    let regex = url.split('?')[1];

    if (!this.dataCache[baseUrl]) {
      this.data = this.fetchData(baseUrl);
      this.dataCache[baseUrl] = this.data;
    } else {
      this.data = this.dataCache[baseUrl];
    }

    return (this.cache[regex] !== undefined) ?
      this.cache[regex] :
      this.data.then((data) => {
        let filtered = data.filter((row, index) => {
          if (index === 0) return row;
          return row[0].match(regex);
        });
        // tranposed the data
        let transposed = filtered[0].map((_col, i) => filtered.map(row => row[i]));
        transposed = this.replaceEmptyString(this.replaceLabel(transposed));

        this.cache[regex] = transposed;
        return transposed;
      });
  }

  replaceEmptyString(transposed) {
    /**
     * Replace empty string value with null
     */
    return transposed.map((ary) => {
      return ary.map((value) => {
        return value === '' || parseFloat(value) == 0 ? null : value;
      })
    })
  }

  private replaceLabel(transposed) {
    let position;
    let lookUp = ['', countyMap, industryMap, workerMap, businessMap]

    /**
     * Find out which value is being used in the compare by filter 
     * and replace the code with the appropriate label.
     */ 
    if (transposed[0][1] === undefined) {
      return transposed;
    } else if (transposed[0][2] === undefined) {
      let one = transposed[0][1].split('');
      for (let i in one) {
        if (parseInt(i) === 0) continue;
        if (one[i] !== 'Z') {
          position = i; break;
        }
      }
    } else {
      let one = transposed[0][1].split('');
      let two = transposed[0][2].split('');
      for (let i in one) {
        if (one[i] !== two[i]) position = i;
      }
    }
    // Actual replacment of the label
    transposed[0] = transposed[0].map((value, index) => {
      if (index === 0) return value;
      return lookUp[position][value[position]];
    })
    return transposed;
  }

  async fetchData(url: string) {
    /**
     * Fetching the data
     */
    let data: any[][];
    let res = await d3Fetch.csv(url);
    data = res.map((row) => res.columns.map((col) => row[col]));
    data.unshift(res.columns);
    return data;
  }
}
