# Workforce Data Source

## workforcesource.ts
This is a custom data source that is compatible with *ccrpc-chart*.  This can registered with the datasource manager by supply an instance of the class, and providing a Regular Expression instance of what url will use this data source.

```javascript
dataSourceManager.registerDataSource(
  new WorkforceSource(),
  new RegExp('.proto')
);
```

## datamap.ts
Create objects of labeling lookup.