import { newSpecPage } from '@stencil/core/testing';
import { App } from './app';

describe('App', () => {
  it('should render the app', async() => {
    const page = await newSpecPage({
      components: [App],
      html: `<wdp-app></wdp-app>`,
    });
    expect(page.root).toBeTruthy;
  });
});
