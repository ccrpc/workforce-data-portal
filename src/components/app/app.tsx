import { h, Component, Element, Prop } from '@stencil/core';
import { countyMap, industryMap, workerMap, businessMap } from '../datasource/datamap';


@Component({
	tag: 'wdp-app',
	styleUrl: 'app.css'
})
export class App {
  /**
   * Options for the compare by filter
   */
	@Prop()
	compare: string[] = [
		'County',
		'Industry',
    'Sex',
    'Race',
    'Ethnicity',
    'Business Type',
    'Business Size'
  ];
  /**
   * Options for the county filter
   */
	@Prop()
	counties: string[] = ['All', ...Object.values(countyMap)];
  /**
   * Options for the industry filter
   */
	@Prop()
	industries: string[] = ['All', ...Object.values(industryMap)];
  /**
   * Options for the worker type filter
   */
	@Prop()
	workerType: string[] = ['All', ...Object.values(workerMap)];
  /**
   * Options for the business type filter
   */
	@Prop()
	businessType: string[] = ['All', ...Object.values(businessMap)];
  /**
   * Title for the page
   */
  @Prop() label: string;
  /**
   * Boolean option for returning custom split pane menu
   */
  @Prop() menu: 'custom' | boolean = true;
  /**
   * Label for menu
   */
  @Prop() menuLabel: string = 'Start Exploring';
  /**
   * Boolean option to ionic split pane
   */
	@Prop() splitPane: boolean = true;

	@Element() el: HTMLElement;

	getMenu() {
		if (this.menu === true) {
			return (
				<ion-menu content-id="main">
					<ion-header>
						<ion-toolbar>
							<ion-title>{this.menuLabel}</ion-title>
						</ion-toolbar>
					</ion-header>
					<ion-content>
						<div class="gl-menu-content">{this.getFilters()}</div>
						<div class="nav">
							<wdp-nav />
						</div>
					</ion-content>
				</ion-menu>
			);
		}
  }
  
	getFilters() {
		return [
			<wdp-scenario />,
			<wdp-advanced-filter>
				<div class='advanced-filter-content'>
					{this.getCompareFilters()} {this.getFilterByFilters()}
				</div>
			</wdp-advanced-filter>
		];
  }
  
	getHeaderButton() {
		return [ <wdp-resource />, <gl-share-button />, <wdp-info-button /> ];
  }
  
	getMain() {
		return (
			<div class="pane-main" id="main">
				<ion-header>
					<ion-toolbar>
						<ion-buttons slot="start">
							{this.getMenuButton()}
							<slot name="start-buttons" />
						</ion-buttons>
						<ion-buttons slot="end">
							{this.getHeaderButton()}
						</ion-buttons>
						<ion-title>{this.label}</ion-title>
					</ion-toolbar>
				</ion-header>
				<ion-content
					scrollX={false}
					scrollY={false}
					class="ion-padding">
					<div class="content" slot="fixed">
						<slot />
					</div>
				</ion-content>
				<slot name="after-content" />
				<ion-footer>
					<slot name="footer" />
				</ion-footer>
			</div>
		);
  }
  
	getMenuButton() {
		if (this.menu) {
			return (
				<ion-menu-toggle>
					<ion-button>
						<ion-icon slot="icon-only" name="menu" />
					</ion-button>
				</ion-menu-toggle>
			);
		}
  }
  
	getCompareFilters() {
		return [
			<ion-item-group class='ion-padding'>
				<ion-item-divider>
					<ion-label>Compare By:</ion-label>
				</ion-item-divider>
				<wdp-compare-by-filter id='compareBy' attribute="compareBy" label="Compare By">
          <ion-label></ion-label>
					{this.compare.map((v) => {
						return (
							<ion-select-option value={v}>
								{v}
							</ion-select-option>
						);
					})}
				</wdp-compare-by-filter>
			</ion-item-group>
		];
  }
  
	getFilterByFilters() {
		return [
			<wdp-filters class="ion-padding" label="Filter By: ">
				<wdp-filter-by-filter class='filterByFilter' label="County" attribute='county'>
					{this.counties.map((county) => {
						return (
							<ion-select-option value={county}>
								{county}
							</ion-select-option>
						);
					})}
				</wdp-filter-by-filter>
				<wdp-filter-by-filter class='filterByFilter' label="Industry" attribute='industry'>
					{this.industries.map((industry) => {
						return (
							<ion-select-option value={industry}>
								{industry}
							</ion-select-option>
						);
					})}
				</wdp-filter-by-filter>
				<wdp-filter-by-filter class='filterByFilter' label="Worker Type" attribute='workerType'>
					{this.workerType.map((type) => {
						return (
							<ion-select-option value={type}>
								{type}
							</ion-select-option>
						);
					})}
				</wdp-filter-by-filter>
				<wdp-filter-by-filter class='filterByFilter' label="Business Type" attribute='businessType'>
					{this.businessType.map((type) => {
						return (
							<ion-select-option value={type}>
								{type}
							</ion-select-option>
						);
					})}
				</wdp-filter-by-filter>
			</wdp-filters>
		];
	}

	render() {
		return (
			<ion-app>
				<ion-split-pane content-id="main">
          {[this.getMenu(), this.getMain()]}
        </ion-split-pane>
			</ion-app>
		);
	}
}
