# wdp-app

This is the main componet for the Workforce data portal application. 

<!-- Auto Generated Below -->


## Properties

| Property       | Attribute    | Description                                         | Type                  | Default                                                                                                            |
| -------------- | ------------ | --------------------------------------------------- | --------------------- | ------------------------------------------------------------------------------------------------------------------ |
| `businessType` | --           | Options for the business type filter                | `string[]`            | `['All', ...Object.values(businessMap)]`                                                                           |
| `compare`      | --           | Options for the compare by filter                   | `string[]`            | `[ 		'County', 		'Industry',     'Sex',     'Race',     'Ethnicity',     'Business Type',     'Business Size'   ]` |
| `counties`     | --           | Options for the county filter                       | `string[]`            | `['All', ...Object.values(countyMap)]`                                                                             |
| `industries`   | --           | Options for the industry filter                     | `string[]`            | `['All', ...Object.values(industryMap)]`                                                                           |
| `label`        | `label`      | Title for the page                                  | `string`              | `undefined`                                                                                                        |
| `menu`         | `menu`       | Boolean option for returning custom split pane menu | `"custom" \| boolean` | `true`                                                                                                             |
| `menuLabel`    | `menu-label` | Label for menu                                      | `string`              | `'Start Exploring'`                                                                                                |
| `splitPane`    | `split-pane` | Boolean option to ionic split pane                  | `boolean`             | `true`                                                                                                             |
| `workerType`   | --           | Options for the worker type filter                  | `string[]`            | `['All', ...Object.values(workerMap)]`                                                                             |


## Dependencies

### Depends on

- ion-menu
- ion-header
- ion-toolbar
- ion-title
- ion-content
- [wdp-nav](../split/navigation)
- [wdp-scenario](../split/scenario)
- [wdp-advanced-filter](../split/advancedfilter)
- [wdp-resource](../header/wdp-resource)
- [wdp-info-button](../header/info)
- ion-buttons
- ion-footer
- ion-menu-toggle
- ion-button
- ion-icon
- ion-item-group
- ion-item-divider
- ion-label
- [wdp-compare-by-filter](../split/comparebyfilter)
- ion-select-option
- [wdp-filters](../split/filters)
- [wdp-filter-by-filter](../split/filterbyfilter)
- ion-app
- ion-split-pane

### Graph
```mermaid
graph TD;
  wdp-app --> ion-menu
  wdp-app --> ion-header
  wdp-app --> ion-toolbar
  wdp-app --> ion-title
  wdp-app --> ion-content
  wdp-app --> wdp-nav
  wdp-app --> wdp-scenario
  wdp-app --> wdp-advanced-filter
  wdp-app --> wdp-resource
  wdp-app --> wdp-info-button
  wdp-app --> ion-buttons
  wdp-app --> ion-footer
  wdp-app --> ion-menu-toggle
  wdp-app --> ion-button
  wdp-app --> ion-icon
  wdp-app --> ion-item-group
  wdp-app --> ion-item-divider
  wdp-app --> ion-label
  wdp-app --> wdp-compare-by-filter
  wdp-app --> ion-select-option
  wdp-app --> wdp-filters
  wdp-app --> wdp-filter-by-filter
  wdp-app --> ion-app
  wdp-app --> ion-split-pane
  ion-menu --> ion-backdrop
  wdp-nav --> ion-list
  wdp-nav --> ion-list-header
  wdp-nav --> ion-item
  ion-item --> ion-icon
  ion-item --> ion-ripple-effect
  wdp-scenario --> ion-button
  wdp-scenario --> ion-item-divider
  wdp-scenario --> ion-picker-controller
  ion-button --> ion-ripple-effect
  wdp-advanced-filter --> ion-item-group
  wdp-advanced-filter --> ion-button
  wdp-resource --> wdp-resource-popover
  wdp-resource --> ion-button
  wdp-resource --> ion-popover-controller
  wdp-resource-popover --> ion-list
  wdp-resource-popover --> ion-list-header
  wdp-resource-popover --> ion-label
  wdp-resource-popover --> ion-item
  wdp-info-button --> wdp-info
  wdp-info-button --> ion-button
  wdp-info-button --> ion-icon
  wdp-info-button --> ion-popover-controller
  wdp-info --> ion-card
  wdp-info --> ion-card-header
  wdp-info --> ion-card-title
  wdp-info --> ion-card-content
  ion-card --> ion-ripple-effect
  wdp-compare-by-filter --> ion-item
  wdp-compare-by-filter --> ion-label
  wdp-compare-by-filter --> ion-select
  wdp-filters --> ion-item-group
  wdp-filters --> ion-item-divider
  wdp-filters --> ion-label
  wdp-filters --> ion-button
  wdp-filter-by-filter --> ion-item
  wdp-filter-by-filter --> ion-label
  wdp-filter-by-filter --> ion-select
  style wdp-app fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
