import { h, Component, Prop, Element, Event, EventEmitter, Listen }
  from '@stencil/core';

@Component({
  tag: 'wdp-compare-by-filter',
  styleUrl: 'comparefilter.css'
})
export class CompareByFilter {
  /**
   * Label for filter
   */
  @Prop() label: string;
  /**
   * The category for the filter
   */
  @Prop() attribute: string;
  /**
   * Label for all options
   */
  @Prop() allOption: string = 'All';
  /**
   * The user selected value for the filter
   */
  @Prop({ mutable: true, reflect: true}) value: number | string;
  /**
   * Boolean for preventing update during internal processing.
   */
  @Prop({ mutable: true}) updateFilterBy: boolean = true;

  @Element() el: HTMLElement;
  /**
   * Event for when individual child filter changed, custom obj as payload
   */
  @Event() compareFilterChanged: EventEmitter;
  /**
   * Event for reseting individual child filter
   */
  @Event() resetFilterByFilter: EventEmitter;

  @Listen('changeFilterValue', {target: 'body'})
  changeFilterValueHandler(e: CustomEvent) {
    this.updateFilterBy = false;
    this.value = e.detail['compareBy']
  }

  changeHandler(e: CustomEvent) {
    let compareByObj = {}
    compareByObj[this.attribute] = e.detail.value
    this.compareFilterChanged.emit(compareByObj);

    this.value = e.detail.value === this.allOption ? undefined : e.detail.value;
    if (this.updateFilterBy) this.resetFilterByFilter.emit();
    this.updateFilterBy = true;
  }

  async componentWillLoad() {
    this.value = this.el.querySelector('ion-select-option').value;
  }

  render() {
    return (
      <ion-item>
        <ion-label>{this.label}</ion-label>
        <ion-select
          value={this.value}
          ok-text="Okay"
          cancel-text="Dismiss"
          onIonChange={(e) => this.changeHandler(e)}
          interfaceOptions={
              {
                cssClass: 'wide-wdp-filter',
              }
          }>
          <slot />
        </ion-select>
      </ion-item>
    )
  }
}
