import { newSpecPage } from '@stencil/core/testing';
import { CompareByFilter } from './comparefilter';

describe('Compare by filter', () => {
  it('should render the compare by filter', async() => {
    const page = await newSpecPage({
      components: [CompareByFilter],
      html: `<wdp-filter></wdp-filter>`,
    });
    expect(page.root).toEqualHtml(
    `<wdp-filter>
    </wdp-filter>`)
  })
})
