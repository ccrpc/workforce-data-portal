# wdp-compare-by-filter

This compoenet render the <wdp-compare-by-filter>

<!-- Auto Generated Below -->


## Properties

| Property         | Attribute          | Description                                               | Type               | Default     |
| ---------------- | ------------------ | --------------------------------------------------------- | ------------------ | ----------- |
| `allOption`      | `all-option`       | Label for all options                                     | `string`           | `'All'`     |
| `attribute`      | `attribute`        | The category for the filter                               | `string`           | `undefined` |
| `label`          | `label`            | Label for filter                                          | `string`           | `undefined` |
| `updateFilterBy` | `update-filter-by` | Boolean for preventing update during internal processing. | `boolean`          | `true`      |
| `value`          | `value`            | The user selected value for the filter                    | `number \| string` | `undefined` |


## Events

| Event                  | Description                                                           | Type               |
| ---------------------- | --------------------------------------------------------------------- | ------------------ |
| `compareFilterChanged` | Event for when individual child filter changed, custom obj as payload | `CustomEvent<any>` |
| `resetFilterByFilter`  | Event for reseting individual child filter                            | `CustomEvent<any>` |


## Dependencies

### Used by

 - [wdp-app](../../app)

### Depends on

- ion-item
- ion-label
- ion-select

### Graph
```mermaid
graph TD;
  wdp-compare-by-filter --> ion-item
  wdp-compare-by-filter --> ion-label
  wdp-compare-by-filter --> ion-select
  ion-item --> ion-icon
  ion-item --> ion-ripple-effect
  wdp-app --> wdp-compare-by-filter
  style wdp-compare-by-filter fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
