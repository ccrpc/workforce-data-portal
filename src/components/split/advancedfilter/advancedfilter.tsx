import { h, Component, Event, EventEmitter, Element, Listen, Prop, State } from '@stencil/core';

@Component({
	tag: 'wdp-advanced-filter'
})
export class WdpAdvancedFilter{
  /**
   * Label for the advance filter
   */
  @Prop() label: string = 'Advanced Filters';
  /**
   * Boolean for toggle button visibility
   */
  @Prop() toggle: boolean = true;
  /**
   * Fire when filter changes, contain the regular expression as data
   */
  @Event() updateContent: EventEmitter;
  @Element() el: HTMLElement;
  
  @State() filters = {
    'compareBy': 'County',
    'county': undefined,
    'industry': undefined,
    'workerType': undefined,
    'businessType': undefined
  }

  @Listen('compareFilterChanged')
  @Listen('filterFilterChanged')
  filterChangedHandler(e: CustomEvent) {
    this.filters = {
      ...this.filters, ...e.detail
    }
    this.updateContent.emit(this.filters);
  }

	handleClick() {
		this.toggle = !this.toggle;
  }
  
	getColor() {
		return this.toggle ? 'secondary' : 'medium';
  }

	setAdvancedFilterVisibibility() {
    //? Need to refactor this to take advantage of using components property
    if (this.toggle) {
      this.el.querySelector('.advanced-filter-content').setAttribute('hidden', '')
    } else {
      this.el.querySelector('.advanced-filter-content').removeAttribute('hidden')
    }
  }

	render() {
    this.setAdvancedFilterVisibibility();
		return (
			<ion-item-group class="ion-padding">
				<ion-button
					size="small"
					color={this.getColor()}
					onClick={() => this.handleClick()}
				>
					{this.label}
				</ion-button>
				<slot name="start" />
				<slot />
				<slot name="end" />
			</ion-item-group>
		);
	}
}

