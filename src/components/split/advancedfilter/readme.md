# wdp-advanced-filter

This is a container component that include <wdp-compare-by-filter>, <wdp-filter-by-filter>.  It handles logic for the advance filter visibility and responsible for listening to any of the changes to the different child filter components, construct the regular expression and fire off an event to the content component. 

<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description                          | Type      | Default              |
| -------- | --------- | ------------------------------------ | --------- | -------------------- |
| `label`  | `label`   | Label for the advance filter         | `string`  | `'Advanced Filters'` |
| `toggle` | `toggle`  | Boolean for toggle button visibility | `boolean` | `true`               |


## Events

| Event           | Description                                                      | Type               |
| --------------- | ---------------------------------------------------------------- | ------------------ |
| `updateContent` | Fire when filter changes, contain the regular expression as data | `CustomEvent<any>` |


## Dependencies

### Used by

 - [wdp-app](../../app)

### Depends on

- ion-item-group
- ion-button

### Graph
```mermaid
graph TD;
  wdp-advanced-filter --> ion-item-group
  wdp-advanced-filter --> ion-button
  ion-button --> ion-ripple-effect
  wdp-app --> wdp-advanced-filter
  style wdp-advanced-filter fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
