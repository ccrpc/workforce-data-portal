import { newSpecPage } from '@stencil/core/testing';
import { WdpNav } from './navigation';

describe('Navigation', () => {
  it('should render the navigation', async() => {
    const page = await newSpecPage({
      components: [WdpNav],
      html: `<wdp-nav></wdp-nav>`,
    });
    expect(page.root).toBeTruthy;
  });
});
