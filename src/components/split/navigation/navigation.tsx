import { h, Component, Prop } from '@stencil/core';

export interface NavLink {
  label: string;
  href: string;
}

@Component({
	tag: 'wdp-nav',
	styleUrl: 'navigation.css'
})
export class WdpNav {
  /**
   * Label for navigation component
   */
	@Prop() label: string = 'Navigate the portal';
  /**
   * Supply nav link objects
   */
  @Prop() supply: NavLink[] = [
    {
      label: 'Households',
      href: '#households'
    },
    {
      label: 'Employment & Unemployment',
      href: '#employment'
    },
    {
      label: 'Earning & Wages',
      href: '#earnings'
    }
  ]
  @Prop() demand: NavLink[] = [
    {
      label: 'Industries',
      href: '#industries'
    },
    {
      label: 'Turnover',
      href: '#turnover'
    },
    {
      label: 'Layoffs',
      href: '#layoffs'
    },
  ]
	render() {
		return (
      [
        <h3 class='ion-padding' id='nav-title'>{this.label}</h3>,
        <ion-list id="side-nav" class="ion-padding">

        <ion-list-header>Workforce Supply</ion-list-header>
        {
        this.supply.map((l) =>
            <ion-item href={l.href}>{l.label}</ion-item>
        )}

        <ion-list-header>Workforce Demand</ion-list-header>
        {this.demand.map((l) =>
            <ion-item href={l.href}>{l.label}</ion-item>
        )}
        </ion-list>
      ]
		);
	}
}
