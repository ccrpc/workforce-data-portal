# wdp-nav



<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description                    | Type        | Default                                                                                                                                                                                                                        |
| -------- | --------- | ------------------------------ | ----------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| `demand` | --        |                                | `NavLink[]` | `[     {       label: 'Industries',       href: '#industries'     },     {       label: 'Turnover',       href: '#turnover'     },     {       label: 'Layoffs',       href: '#layoffs'     },   ]`                            |
| `label`  | `label`   | Label for navigation component | `string`    | `'Navigate the portal'`                                                                                                                                                                                                        |
| `supply` | --        | Supply nav link objects        | `NavLink[]` | `[     {       label: 'Households',       href: '#households'     },     {       label: 'Employment & Unemployment',       href: '#employment'     },     {       label: 'Earning & Wages',       href: '#earnings'     }   ]` |


## Dependencies

### Used by

 - [wdp-app](../../app)

### Depends on

- ion-list
- ion-list-header
- ion-item

### Graph
```mermaid
graph TD;
  wdp-nav --> ion-list
  wdp-nav --> ion-list-header
  wdp-nav --> ion-item
  ion-item --> ion-icon
  ion-item --> ion-ripple-effect
  wdp-app --> wdp-nav
  style wdp-nav fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
