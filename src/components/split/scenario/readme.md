# wdp-scenario



<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description                           | Type     | Default             |
| -------- | --------- | ------------------------------------- | -------- | ------------------- |
| `label`  | `label`   | Label for report selector             | `string` | `'Choose a report'` |
| `value`  | `value`   | User selected value for the component | `string` | `undefined`         |


## Events

| Event               | Description                                                                          | Type               |
| ------------------- | ------------------------------------------------------------------------------------ | ------------------ |
| `changeFilterValue` | Fire event that mimic the options for changed for compareByFilter and filterByFilter | `CustomEvent<any>` |


## Dependencies

### Used by

 - [wdp-app](../../app)

### Depends on

- ion-button
- ion-item-divider
- ion-picker-controller

### Graph
```mermaid
graph TD;
  wdp-scenario --> ion-button
  wdp-scenario --> ion-item-divider
  wdp-scenario --> ion-picker-controller
  ion-button --> ion-ripple-effect
  wdp-app --> wdp-scenario
  style wdp-scenario fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
