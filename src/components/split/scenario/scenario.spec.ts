import { newSpecPage } from '@stencil/core/testing';
import { WdpScenario } from './scenario';

describe('Scenario', () => {
  it('should render the scenario', async() => {
    const page = await newSpecPage({
      components: [WdpScenario],
      html: `<wdp-scenario></wdp-scenario>`,
    });
    expect(page.root).toBeTruthy;
  });
});
