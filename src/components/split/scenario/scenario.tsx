import { h, Component, Event, EventEmitter, Prop } from '@stencil/core';

@Component({
	tag: 'wdp-scenario',
	styleUrl: 'scenario.css'
})
export class WdpScenario {
  /**
   * Label for report selector
   */
  @Prop() label: string = 'Choose a report';
  /**
   * User selected value for the component
   */
  @Prop({ mutable: true }) value: string;
  /**
   * Ionic picker component controller
   */
	@Prop({ connect: 'ion-picker-controller' }) pickerCtrl: HTMLIonPickerControllerElement;
	/**
   * Fire event that mimic the options for changed for compareByFilter and filterByFilter
   */
	@Event() changeFilterValue: EventEmitter;

	options = {
		'Explore by county': {
			'compareBy': 'County',
		},
		'Explore by sex': {
			'compareBy': 'Sex',
			'county': 'Champaign',
    },
    'Explore by race': {
      'compareBy': 'Race',
      'county': 'Champaign',
    },
		'Explore by business size': {
			'compareBy': 'Business Size',
			'county': 'Champaign',
		}
	};

	changeHandler(e: CustomEvent) {
		this.value = e.detail.value;
		this.changeFilterValue.emit(this.options[this.value])
	}

	// https://github.com/ionic-team/ionic-docs/blob/master/src/demos/api/picker/index.html
	// getColumns and getColumnOptions are taken from the above name.
	getColumns(numColumns, numOptions, columnOptions) {
		let columns = [];
		for (let i = 0; i < numColumns; i++) {
			columns.push({
				name: `col-${i}`,
				options: this.getColumnOptions(i, numOptions, columnOptions)
			});
		}

		return columns;
	}

	getColumnOptions(columnIndex, numOptions, columnOptions) {
		let options = [];
		for (let i = 0; i < numOptions; i++) {
			options.push({
				text: columnOptions[columnIndex][i % numOptions],
				value: i
			})
		}

		return options;
	}

	async clickHandler(_e) {
		const picker = await this.pickerCtrl.create({
			columns: this.getColumns(1, 5, [Object.keys(this.options)]),
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel'
          },
          {
            text: 'Confirm',
            handler: (value) => {
              this.changeFilterValue.emit(this.options[value['col-0'].text])
            }
          }
        ]
		});
		await picker.present();
		return picker;
	}

	render() {
		return (
			[
				<ion-button class='ion-padding' expand='full' color='primary' onClick={(e) => this.clickHandler(e)}>Choose a report</ion-button>,
				<ion-item-divider></ion-item-divider>
			]
		);
	}
}