import { h, Component, Element, Event, Listen, Prop } from '@stencil/core';
import { EventEmitter } from '@ionic/core/dist/types/stencil.core';

@Component({
	tag: 'wdp-filters'
})
export class Filters {
  /**
   * Label for filter by filters
   */
  @Prop() label: string = '';
  /**
   * Boolean option for a reset button to render
   */
  @Prop() reset: boolean = true;
	@Element() el: HTMLElement;
  /**
   * Event to handle individual filter reset
   */
  @Event() resetFilter: EventEmitter;

  @Listen('resetFilterByFilter', {target: 'body'})
  handleResetFilterByFilter() {
    this.handleReset()
  }

	handleReset() {
    this.resetFilter.emit();
  }

	render() {
		return (
			<ion-item-group class="ion-padding">
				<ion-item-divider>
					<ion-label>{this.label}</ion-label>
				</ion-item-divider>
				<slot name="start" />
				<slot />
				<slot name="end" />
				{this.reset ? (
					<ion-button
						expand="full"
						onClick={() => this.handleReset()}
					>
						Reset
					</ion-button>
				) : (
					''
				)}
			</ion-item-group>
		);
	}
}
