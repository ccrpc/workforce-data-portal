import { newSpecPage } from '@stencil/core/testing';
import { Filters } from './filters';

describe('Filters', () => {
  it('should render the filters', async() => {
    const page = await newSpecPage({
      components: [Filters],
      html: `<wdp-filters></wdp-filters>`,
    });
    expect(page.root).toEqualHtml(
    `<wdp-filters>
      <ion-item-group class="ion-padding">
        <ion-item-divider>
          <ion-label></ion-label>
        </ion-item-divider>
        <ion-button expand="full">
          Reset
        </ion-button>
      </ion-item-group>
    </wdp-filters>`)
  });

  it('should should have correct label', async () => {
    const page = await newSpecPage({
      components: [Filters],
      html: `<wdp-filters></wdp-filters>`,
    });
    expect(page.rootInstance.label).toEqual('');
    let label = 'Test Label';
    page.rootInstance.label = label;
    expect(page.root.label).toEqual(label);
  });

  it('should should have correct reset', async () => {
    const page = await newSpecPage({
      components: [Filters],
      html: `<wdp-filters></wdp-filters>`,
    });
    expect(page.rootInstance.reset).toBeTruthy();
    page.root.reset = false;
    expect(page.rootInstance.reset).toBeFalsy();
  });
});
