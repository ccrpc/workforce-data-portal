# wdp-filters

This component contains the individual <wdp-filter-by-filter> components and handle the reset logic.

<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description                                 | Type      | Default |
| -------- | --------- | ------------------------------------------- | --------- | ------- |
| `label`  | `label`   | Label for filter by filters                 | `string`  | `''`    |
| `reset`  | `reset`   | Boolean option for a reset button to render | `boolean` | `true`  |


## Events

| Event         | Description                             | Type               |
| ------------- | --------------------------------------- | ------------------ |
| `resetFilter` | Event to handle individual filter reset | `CustomEvent<any>` |


## Dependencies

### Used by

 - [wdp-app](../../app)

### Depends on

- ion-item-group
- ion-item-divider
- ion-label
- ion-button

### Graph
```mermaid
graph TD;
  wdp-filters --> ion-item-group
  wdp-filters --> ion-item-divider
  wdp-filters --> ion-label
  wdp-filters --> ion-button
  ion-button --> ion-ripple-effect
  wdp-app --> wdp-filters
  style wdp-filters fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
