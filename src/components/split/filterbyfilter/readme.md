# wdp-filter-by-filter

This renders the <wdp-filter-by-filter> component.

<!-- Auto Generated Below -->


## Properties

| Property    | Attribute    | Description                            | Type               | Default     |
| ----------- | ------------ | -------------------------------------- | ------------------ | ----------- |
| `allOption` | `all-option` | Label for all options                  | `string`           | `'All'`     |
| `attribute` | `attribute`  | The category for the filter            | `string`           | `undefined` |
| `label`     | `label`      | Label for the individual filter        | `string`           | `undefined` |
| `value`     | `value`      | The user selected value for the filter | `number \| string` | `undefined` |


## Events

| Event                 | Description                                                              | Type               |
| --------------------- | ------------------------------------------------------------------------ | ------------------ |
| `filterFilterChanged` | Event for when individual child filter changed, filter values as payload | `CustomEvent<any>` |


## Dependencies

### Used by

 - [wdp-app](../../app)

### Depends on

- ion-item
- ion-label
- ion-select

### Graph
```mermaid
graph TD;
  wdp-filter-by-filter --> ion-item
  wdp-filter-by-filter --> ion-label
  wdp-filter-by-filter --> ion-select
  ion-item --> ion-icon
  ion-item --> ion-ripple-effect
  wdp-app --> wdp-filter-by-filter
  style wdp-filter-by-filter fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
