import { newSpecPage } from '@stencil/core/testing';
import { FilterByFilter } from './filterfilter';

describe('Filter by Filter', () => {
  it('should render filter by filter', async() => {
    const page = await newSpecPage({
      components: [FilterByFilter],
      html: `<wdp-filter></wdp-filter>`,
    });
    expect(page.root).toEqualHtml(
    `<wdp-filter>
    </wdp-filter>`)
  })
})
