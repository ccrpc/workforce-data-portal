import { h, Component, Prop, Element, Event, EventEmitter, Listen }
  from '@stencil/core';

@Component({
  tag: 'wdp-filter-by-filter',
  styleUrl: 'filterfilter.css'
})
export class FilterByFilter {
  /**
   * Label for the individual filter
   */
  @Prop() label: string;
  /**
   * The category for the filter
   */
  @Prop() attribute: string;
  /**
   * Label for all options
   */
  @Prop() allOption: string = 'All';
  /**
   * The user selected value for the filter
   */
  @Prop({ mutable: true, reflect: true}) value: number | string;

  @Element() el: HTMLElement;
  /**
   * Event for when individual child filter changed, filter values as payload
   */
  @Event() filterFilterChanged: EventEmitter;

  @Listen('changeFilterValue', { target: 'body' })
  changeFilterValueHandler(e: CustomEvent) {
    this.value = e.detail[this.attribute]
  }

  @Listen('resetFilter', { target: 'body' })
  resetFilterHandler() {
    this.value = undefined;
  }

  changeHandler(e) {
    this.value = e.detail.value === this.allOption ? undefined : e.detail.value;
    let filtersValue = {};
    filtersValue[this.attribute] = this.value;
    this.filterFilterChanged.emit(filtersValue);
  }

  render() {
    return (
      <ion-item>
        <ion-label>{this.label}</ion-label>
        <ion-select
          value={this.value}
          ok-text="Okay"
          cancel-text="Dismiss"
          onIonChange={(e) => this.changeHandler(e)}
          interfaceOptions={
              {
                cssClass: 'wide-wdp-filter',
              }
          }>
          <slot />
        </ion-select>
      </ion-item>
    )
  }
}
