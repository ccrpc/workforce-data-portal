import { countyMap, industryMap, workerMap, businessMap } from '../../datasource/datamap';

export let regexMap = {};

let objs = [countyMap, industryMap, workerMap, businessMap];

objs.map((obj) => {
  for (let key in obj) {
    regexMap[obj[key]] = key;
  };
});

regexMap['undefined'] = 'Z';

