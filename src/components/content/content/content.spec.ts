import { newSpecPage } from '@stencil/core/testing';
import { WpdContent } from './content';

describe('Content', () => {
  it('should render the content', async() => {
    const page = await newSpecPage({
      components: [WpdContent],
      html: `<wdp-content></wdp-content>`,
    });
    expect(page.root).toBeTruthy;
  });
});
