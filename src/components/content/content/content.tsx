import { h, Component, Listen, State } from '@stencil/core';
import { dataSourceManager } from '@ccrpc/charts';
import { WorkforceSource } from '../../datasource/workforcesource';
import { regexMap } from './regexmap';


@Component({
  tag: 'wdp-content',
  styleUrl: 'content.css'
})
export class WpdContent {
  @State() filters = {
    'compareBy': 'County',
    'county': undefined,
    'industry': undefined,
    'workerType': undefined,
    'businessType': undefined
  }

  @State() regex: string = '[CDIFPV]ZZZ';

  @Listen('updateContent', { target: 'body' })
  updateContentHandler(e: CustomEvent) {
    this.filters = e.detail;
    this.regex = this.constructRegex();
  }

  constructRegex() {
    let regex: string;
    switch (this.filters['compareBy']) {
      case 'County':
        regex = (this.filters['county'] === undefined
          ? '[CDIFPV]' : l(this.filters['county']))
          + l(this.filters['industry']) 
          + l(this.filters['workerType'])
          + l(this.filters['businessType']);
        break;
      case 'Industry':
        regex = l(this.filters['county'])
        + (this.filters['industry'] === undefined
          ? '[A-Y0-9]' : l(this.filters['industry']))
        + l(this.filters['workerType'])
        + l(this.filters['businessType']);
        break;
      case 'Sex':
        regex = l(this.filters['county']) 
        + l(this.filters['industry'])
        + (this.filters['workerType'] === undefined
          ? '[M|F]' : l(this.filters['workerType']))
        + l(this.filters['businessType'])
        break;
      case 'Race':
        regex = l(this.filters['county']) 
        + l(this.filters['industry'])
        + (this.filters['workerType'] === undefined
          ? '[W|B|N|A|P|T]' : l(this.filters['workerType']))
        + l(this.filters['businessType'])
        break;
      case 'Ethnicity':
        regex = l(this.filters['county']) 
        + l(this.filters['industry'])
        + (this.filters['workerType'] === undefined
          ? '[H|K]' : l(this.filters['workerType']))
        + l(this.filters['businessType'])
        break;
      case 'Business Type':
        regex = l(this.filters['county']) 
        + l(this.filters['industry'])
        + l(this.filters['workerType'])
        + (this.filters['businessType'] === undefined
          ? '[C|P]' : l(this.filters['businessType']))
        break;
      case 'Business Size':
        regex = l(this.filters['county']) 
        + l(this.filters['industry'])
        + l(this.filters['workerType'])
        + (this.filters['businessType'] === undefined
          ? '[0-9]' : l(this.filters['businessType']))
        break;
    }
    return regex;
  }

  getWorkforceSupply() {
    return (
      <div id="supply">
        <slot name="supply-start" />
        {this.getHousehold()}
        {this.getEmployment()}
        {this.getEarning()}
        <slot name="supply-end" />
      </div>
    );
  }

  getHousehold() {
    return (
      <div id="households">
        <slot name="households-start" />
        <wdp-chart
          chart-code="H"
          filters={this.filters}
          url={`assets/workforce.proto?H${this.regex}`}
          chart-title="Households Counts"
          x-label="Year"
          y-label="Households"
          type="line"
          source="U.S. Census Bureau - American Community Survey"
          source-url="https://www.census.gov/programs-surveys/acs"
          width="100%"
        />
        <wdp-chart
          chart-code="I"
          filters={this.filters}
          url={`assets/workforce.proto?I${this.regex}`}
          chart-title="Median Household Income"
          x-label="Year"
          y-label="Households"
          type="line"
          source="U.S. Census Bureau - American Community Survey"
          source-url="https://www.census.gov/programs-surveys/acs"
          width="100%"
        />
        <slot name="households-end" />
      </div>
    );
  }

  getEmployment() {
    return (
      <div id="employment">
        <slot name="employment-start" />
        <wdp-chart
          chart-code="E"
          filters={this.filters}
          url={`assets/workforce.proto?E${this.regex}`}
          chart-title="Employment"
          x-label="Year"
          y-label="Employment"
          type="line"
          source="U.S. Census Quarterly Workforce Indicators"
          source-url="https://qwiexplorer.ces.census.gov"
          width="100%"
        >
        </wdp-chart>

        <slot name="employment-end" />
        <slot name="labor-force-start" />
        <wdp-chart
          chart-code="L"
          filters={this.filters}
          url={`assets/workforce.proto?L${this.regex}`}
          chart-title="Labor Force"
          x-label="County"
          y-label="Individuals"
          type="line"
          legend-position="right"
          aspect-ratio="3"
          source="Bureau of Labor Statistics - Local Area Unemployment Statistics"
          source-url="https://www.bls.gov/lau/"
          width="100%"
        />
        <wdp-chart
          chart-code="U"
          filters={this.filters}
          url={`assets/workforce.proto?U${this.regex}`}
          chart-title="Unemployment Individual"
          x-label="County"
          y-label="Individuals"
          type="line"
          legend-position="right"
          aspect-ratio="3"
          source=" Local Area Unemployment Statistics"
          source-url="https://www.bls.gov/lau/"
          width="100%"
        />
        <slot name="labor-force-end" />
      </div>
    );
  }

  getEarning() {
    return (
      <div id="earnings">
        <slot name="earnings-start" />
        <wdp-chart
          chart-code="R"
          filters={this.filters}
          url={`assets/workforce.proto?R${this.regex}`}
          chart-title="Average Monthly Earnings"
          x-label="Year"
          y-label="Average Monthly Earnings"
          type="line"
          source="U.S. Census Quarterly Workforce Indicators"
          source-url="https://qwiexplorer.ces.census.gov"
          width="100%"
        />
        <slot name="earnings-end" />
        <slot name="pay-start" />
        <wdp-chart
          chartCode="A"
          filters={this.filters}
          url={`assets/workforce.proto?A${this.regex}`}
          chart-title="Average Annual Pay"
          x-label="Year"
          y-label="Average Annual Pay"
          type='line'
          source="Bureau of Labor Statistics - Quarterly Census of Employment and Wages"
          source-url="https://www.bls.gov/cew/"
          width="100%"
        />
        <slot name="pay-end" />
      </div>
    );
  }

  getWorkforceDemand() {
    return (
      <div id="demand">
        <slot name="demand-start" />
        {this.getIndustry()}
        {this.getTurnOver()}
        {this.getLayoff()}
        <slot name="demand-end" />
      </div>
    );
  }

  getIndustry() {
    return (
      <div id="industries">
        <slot name="industries-start" />
        <wdp-chart
          chart-code="S"
          filters={this.filters}
          url={`assets/workforce.proto?S${this.regex}`}
          chart-title="Average Annual Establishments"
          x-label="Year"
          y-label="Average Annual Establishments"
          type="line"
          source="Bureau of Labor Statistics - Quarterly Census of Employment and Wages"
          source-url="https://www.bls.gov/cew/"
          width="100%"
        />
        <slot name="industries-end" />
        <slot name="wages-start" />
        <wdp-chart
          chart-code="P"
          filters={this.filters}
          url={`assets/workforce.proto?P${this.regex}`}
          chart-title="Total Annual Wages in the Workforce Area"
          type="line"
          source="Bureau of Labor Statistics - Quarterly Census of Employment and Wages"
          source-url="https://www.bls.gov/cew/"
          width="100%"
        />
        <slot name="wages-end" />
      </div>
    );
  }

  getTurnOver() {
    return (
      <div id="turnover">
        <slot name="turnover-start" />
        <wdp-chart
          chart-code="T"
          filters={this.filters}
          url={`assets/workforce.proto?T${this.regex}`}
          chart-title="Turnover Rate"
          x-label="Year"
          y-label="Turnover Rate"
          type="line"
          source="U.S. Census Quarterly Workforce Indicators"
          source-url="https://qwiexplorer.ces.census.gov"
          width="100%"
        />
        <slot name="turnover-end" />
        <slot name="firm-job-change-start" />
        <wdp-chart
          chart-code="C"
          filters={this.filters}
          url={`assets/workforce.proto?C${this.regex}`}
          chart-title="Firm Job Change"
          x-label="Year"
          y-label="Firm Job Change"
          type="line"
          source="U.S. Census Quarterly Workforce Indicators"
          source-url="https://qwiexplorer.ces.census.gov"
          width="100%"
        >
          <rpc-dataset
            label="No Change"
            type="line"
            data="0,0,0,0,0,0,0,0,0,0,0"
            fill="false"
            border-color="grey"
            point-radius="0"
          />
        </wdp-chart>
        <slot name="firm-job-change-end" />
      </div>
    );
  }
  
  getLayoff() {
    return (
      <div id="layoffs">
        <slot name="layoffs-start" />
        <wdp-chart
          chart-code="W"
          filters={this.filters}
          url={`assets/warn.proto?W${this.regex}`}
          chart-title="WARN Layoff Data"
          source="Illinois Notices of Layoffs and Closures (WARN)"
          source-url="https://www2.illinois.gov/dceo/WorkforceDevelopment/warn/Pages/default.aspx"
          text-alignment="l,r"
          width="100%"
        />
        <slot name="layoffs-end" />
      </div>
    );
  }
  
  registeredWorkforceSource() {
    dataSourceManager.registerDataSource(
      new WorkforceSource(),
      new RegExp('.proto')
    );
  }

  componentWillLoad() {
    this.registeredWorkforceSource()
  }

  render() {
    return [
      <div id="main-content" class="ion-padding">
        {this.getWorkforceSupply()}
        {this.getWorkforceDemand()}
      </div>
    ];
  }
}

function l(v) {
  return regexMap[v];
}