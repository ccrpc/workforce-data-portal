# wdp-content

This is a container component that contains charts and written description.

<!-- Auto Generated Below -->


## Dependencies

### Depends on

- [wdp-chart](../chart)
- rpc-dataset

### Graph
```mermaid
graph TD;
  wdp-content --> wdp-chart
  wdp-content --> rpc-dataset
  wdp-chart --> rpc-chart
  rpc-chart --> rpc-table
  rpc-chart --> rpc-metadata
  rpc-table --> rpc-metadata
  style wdp-content fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
