import { newSpecPage } from '@stencil/core/testing';
import { WdpChart } from './chart';

describe('Chart', () => {
  it('should render the chart', async() => {
    const page = await newSpecPage({
      components: [WdpChart],
      html: `<wdp-chart></wdp-chart>`,
    });
    expect(page.root).toBeTruthy;
  });
});
