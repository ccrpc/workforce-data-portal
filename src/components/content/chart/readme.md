# wdp-chart

This a wrapper component for the <rpc-chart> component.  This component generates a no data message based on the regular expression provided.

<!-- Auto Generated Below -->


## Properties

| Property     | Attribute     | Description                                                          | Type       | Default                                                                                                                                                                                                       |
| ------------ | ------------- | -------------------------------------------------------------------- | ---------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `chartCode`  | `chart-code`  | Code for the chart                                                   | `string`   | `undefined`                                                                                                                                                                                                   |
| `chartTitle` | `chart-title` | Chart title                                                          | `string`   | `undefined`                                                                                                                                                                                                   |
| `colors`     | --            | Color for the wdp chart                                              | `string[]` | `[     'red', 'blue', 'lime', 'orange', 'indigo', 'green',     'violet', 'yellow', 'gray', 'brown', 'bisque', 'chartreuse',     'aqua', 'deeppink', 'gold', 'mistyrose', 'lightcyan',     'maroon', 'olive']` |
| `columns`    | `columns`     | columns parameter pass to ccrpc-chart                                | `string`   | `undefined`                                                                                                                                                                                                   |
| `filters`    | `filters`     |                                                                      | `any`      | `{}`                                                                                                                                                                                                          |
| `regex`      | `regex`       | Regulation express for the data, use for generating no data messages | `string`   | `''`                                                                                                                                                                                                          |
| `source`     | `source`      | Source text to display for the chart                                 | `string`   | `undefined`                                                                                                                                                                                                   |
| `sourceUrl`  | `source-url`  | Source url link                                                      | `string`   | `undefined`                                                                                                                                                                                                   |
| `type`       | `type`        | Chart Type (see ccrpc-chart)                                         | `string`   | `undefined`                                                                                                                                                                                                   |
| `url`        | `url`         | Url for query the data                                               | `string`   | `undefined`                                                                                                                                                                                                   |
| `width`      | `width`       | Width property for Chart.js                                          | `string`   | `'100%'`                                                                                                                                                                                                      |
| `xLabel`     | `x-label`     | Label for x axis                                                     | `string`   | `undefined`                                                                                                                                                                                                   |
| `yLabel`     | `y-label`     | Label for y axis                                                     | `string`   | `undefined`                                                                                                                                                                                                   |


## Dependencies

### Used by

 - [wdp-content](../content)

### Depends on

- rpc-chart

### Graph
```mermaid
graph TD;
  wdp-chart --> rpc-chart
  rpc-chart --> rpc-table
  rpc-chart --> rpc-metadata
  rpc-table --> rpc-metadata
  wdp-content --> wdp-chart
  style wdp-chart fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
