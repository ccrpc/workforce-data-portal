import { h, Component, Element, Prop } from '@stencil/core';
import { countyMap, workerMap, industryMap, businessMap } from '../../datasource/datamap';


const allBusiness = [];
for (let k in businessMap) {
  allBusiness.push(businessMap[k]);
};

const businessSize = [];
for (let k in businessMap) {
  if (k !== 'P' && k !== 'C') businessSize.push(businessMap[k]);
};

const businessTypes = [];
for (let k in businessMap) {
  if (k === 'P' || k === 'C') businessTypes.push(businessMap[k]);
};

const filtersCheck = {
  'H': {
    'compareBy': ['County'],
    'county': [undefined, ...Object.values(countyMap)],
    'industry': [undefined],
    'workerType': [undefined],
    'businessType': [undefined]
  },
  'I': {
    'compareBy': ['County'],
    'county': [undefined, ...Object.values(countyMap)],
    'industry': [undefined],
    'workerType': [undefined],
    'businessType': [undefined]
  },
  'R': {
    'compareBy': ['County', 'Industry', 'Sex', 'Race', 'Ethnicity', 'Business Type', 'Business Size'],
    'county': [...Object.values(countyMap)],
    'industry': [...Object.values(industryMap)],
    'workerType': [...Object.values(workerMap)],
    'businessType': [...allBusiness]
  },
  'L': {
    'compareBy': ['County'],
    'county': [undefined, ...Object.values(countyMap)],
    'industry': [undefined],
    'workerType': [undefined],
    'businessType': [undefined]
  },
  'U': {
    'compareBy': ['County'],
    'county': [undefined, ...Object.values(countyMap)],
    'industry': [undefined],
    'workerType': [undefined],
    'businessType': [undefined]
  },
  'S': {
    'compareBy': ['County', 'Industry', 'Business Type'],
    'county': [undefined, ...Object.values(countyMap)],
    'industry': [undefined, ...Object.values(industryMap)],
    'workerType': [undefined],
    'businessType': [undefined, ...businessTypes]
  },
  'P': {
    'compareBy': ['County', 'Industry', 'Business Type'],
    'county': [undefined, ...Object.values(countyMap)],
    'industry': [undefined, ...Object.values(industryMap)],
    'workerType': [undefined],
    'businessType': [undefined, ...businessTypes]
  },
  'A': {
    'compareBy': ['County', 'Industry', 'Business Type'],
    'county': [undefined, ...Object.values(countyMap)],
    'industry': [undefined, ...Object.values(industryMap)],
    'workerType': [undefined],
    'businessType': [undefined, ...businessTypes]
  },
  'E': {
    'compareBy': ['County', 'Industry', 'Sex', 'Race', 'Ethnicity', 'Business Type', 'Business Size'],
    'county': [undefined, ...Object.values(countyMap)],
    'industry': [undefined, ...Object.values(industryMap)],
    'workerType': [undefined, ...Object.values(workerMap)],
    'businessType': [undefined, ...allBusiness]
  },
  'T': {
    'compareBy': ['County', 'Industry', 'Business Type', 'Business Size'],
    'county': [undefined, ...Object.values(countyMap)],
    'industry': [undefined, ...Object.values(industryMap)],
    'workerType': [undefined],
    'businessType': [undefined, ...allBusiness]
  },
  'C': {
    'compareBy': ['County', 'Industry', 'Business Type', 'Business Size'],
    'county': [undefined, ...Object.values(countyMap)],
    'industry': [undefined, ...Object.values(industryMap)],
    'workerType': [undefined],
    'businessType': [undefined, ...allBusiness]
  },
  'W': {
    'compareBy': ['County', 'Industry'],
    'county': [undefined, ...Object.values(countyMap)],
    'workerType': [undefined],
    'businessType': [undefined],
  }
};

const messageLookUp = {
  'H': {
    'compareBy': 'Set compare by filter to "County". ',
    'industry': 'Set industry to "All". ',
    'workerType': 'Set worker type to "All". ',
    'businessType': 'Set business type to "All". ',
  },
  'I': {
    'compareBy': 'Set compare by filter to "County". ',
    'industry': 'Set industry to "All". ',
    'workerType': 'Set worker type to "All". ',
    'businessType': 'Set business type to "All". ',
  },
  'R': {
    'county': 'Select a county. ',
    'industry': 'Select an industry. ',
    'workerType': 'Select a worker type. ',
    'businessType': 'Select a business type. ',
  },
  'L': {
    'compareBy': 'Set compare by filter to "County". ',
    'industry': 'Set industry to "All". ',
    'workerType': 'Set worker type to "All". ',
    'businessType': 'Set business type to "All". ',
  },
  'U': {
    'compareBy': 'Set compare by filter to "County". ',
    'industry': 'Set industry to "All". ',
    'workerType': 'Set worker type to "All". ',
    'businessType': 'Set business type to "All". ',
  },
  'P': {
    'compareBy': 'This data set is not available by "Worker Type". Set compare by filter to \'County\', \'Industry\', or \'Business Type\' to see a result. ',
    'workerType': 'Set worker type to "All". ',
    'businessType': 'Set business type to "All" ',
  },
  'S': {
    'compareBy': 'This data set is not available by "Worker Type". Set compare by filter to \'County\', \'Industry\', or \'Business Type\' to see a result. ',
    'workerType': 'Set worker type to "All". ',
    'businessType': 'Set business type to "All" ',
  },
  'A': {
    'compareBy': 'This data set is not available by "Worker Type". Set compare by filter to \'County\', \'Industry\', or \'Business Type\' to see a result. ',
    'industry': 'Set industry to "All". ',
    'workerType': 'Set worker type to "All". ',
    'businessType': 'Set business type to "All" ',
  },
  'T': {
    'compareBy': 'This data set is not available by "Worker Type". Set compare by filter to \'County\', \'Industry\', or \'Business Type\' to see a result. ',
    'workerType': 'Set worker type to "All". ',
  },
  'C': {
    'compareBy': 'This data set is not available by "Worker Type". Set compare by filter to \'County\', \'Industry\', or \'Business Type\' to see a result. ',
    'workerType': 'Set worker type to "All". ',
  },
  'E': {

  },
  'W': {
    'compareBy': 'Set compare by filter to "Counties" or "Industry". ',
    'workerType': 'Set worker type to "All". ',
    'businessType': 'Set business type to "All". ',
  }
};

@Component({
  tag: 'wdp-chart',
  styleUrl: 'chart.css'
})
export class WdpChart {
  /**
   * Regulation express for the data, use for generating no data messages
   */
  @Prop() regex: string = '';
  /**
   * Code for the chart
   */
	@Prop() chartCode: string;
  /**
   * Url for query the data
   */
  @Prop() url: string;
  /**
   * Chart title
   */
  @Prop() chartTitle: string;
  /**
   * Label for x axis
   */
  @Prop() xLabel: string;
  /**
   * Label for y axis
   */
  @Prop() yLabel: string;
  /**
   * Chart Type (see ccrpc-chart)
   */
  @Prop() type: string;
  /**
   * columns parameter pass to ccrpc-chart
   */
  @Prop() columns?: string; 
  /**
   * Source text to display for the chart
   */
  @Prop() source: string;
  /**
   * Source url link
   */
  @Prop() sourceUrl: string;
  /**
   * Width property for Chart.js
   */
  @Prop() width: string = '100%';
  /**
   * Color for the wdp chart
   */
  @Prop() colors: string[] = [
    'red', 'blue', 'lime', 'orange', 'indigo', 'green',
    'violet', 'yellow', 'gray', 'brown', 'bisque', 'chartreuse',
    'aqua', 'deeppink', 'gold', 'mistyrose', 'lightcyan',
    'maroon', 'olive'];

	@Prop() filters: any = {};

	@Element() el: HTMLElement;
  invalidFilter = [];


	constructMessage() {
    let messages = [`Please do the following to see data for ${this.chartTitle}. `]
    for (let k of this.invalidFilter) {
      messages.push(messageLookUp[this.chartCode][k])
    }
    this.invalidFilter = []; 
    return messages.join(' ');
	}

  checkChartValidity() {
    for (let key in filtersCheck[this.chartCode]) {
      if (filtersCheck[this.chartCode][key].indexOf(this.filters[key]) === -1) {
        this.invalidFilter.push(key) 
      }
    }
    return (this.invalidFilter.length > 0) ? false : true;
  }

	render() {
		if (this.checkChartValidity()) return (
			<rpc-chart
        url={this.url}
        colors={this.colors}
				chart-title={this.chartTitle}
				x-label={this.xLabel}
				y-label={this.yLabel}
				type={this.type}
				columns={this.columns}
        source={this.source}
        source-url={this.sourceUrl}
				width={this.width}
			>
				<slot />
			</rpc-chart>
		)

		return (
      <div class='aspect-ratio-box'>
        <div class='aspect-ratio-box-inside'>
          <div class='flex-box-centering'>
            <div class='sizing'>
                {this.constructMessage()}
            </div>
          </div>
        </div>
      </div>

		)
	}
}
