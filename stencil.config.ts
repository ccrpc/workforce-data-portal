import { Config } from '@stencil/core';

// https://stenciljs.com/docs/config

export const config: Config = {
    namespace: 'wdp',
    outputTargets: [{
        type: 'www',
        baseUrl: '/wdp',
        serviceWorker: null
    }],
    globalScript: 'src/global/app.ts',
    globalStyle: 'src/global/app.css'
};
