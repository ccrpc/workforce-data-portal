# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.1.2](https://gitlab.com/ccrpc/workforce-data-portal/compare/v0.1.1...v0.1.2) (2020-09-01)


### Features

* **header:** Add external resource popup. ([22d8d5d](https://gitlab.com/ccrpc/workforce-data-portal/commit/22d8d5d352d41776479ddb54c76aded0364169a6)), closes [#40](https://gitlab.com/ccrpc/workforce-data-portal/issues/40)
* **nav:** Update navigation links ([0f8af8a](https://gitlab.com/ccrpc/workforce-data-portal/commit/0f8af8aac93333a69ea2991247e45cf4582c3eb8)), closes [#41](https://gitlab.com/ccrpc/workforce-data-portal/issues/41)

### 0.1.1 (2020-07-29)


### Features

* **chart:** Connect all filters to chart ([e58efe0](https://gitlab.com/ccrpc/workforce-data-portal/commit/e58efe0d3c2d3a5e930dd5f08eebfea057477194))
* **chart:** Connect QWI dataset ([bd48e7a](https://gitlab.com/ccrpc/workforce-data-portal/commit/bd48e7a0723cc6d483ed5b19e15e947296919049))
* **chart:** Fix business type message ([8ed8730](https://gitlab.com/ccrpc/workforce-data-portal/commit/8ed87303c7025823cb7a02dbae8a2aa47517519b)), closes [#32](https://gitlab.com/ccrpc/workforce-data-portal/issues/32)
* **chart:** Improve message for filter choice. ([02fa31c](https://gitlab.com/ccrpc/workforce-data-portal/commit/02fa31cecf91271900b992d1d1ff5147e0bc0611)), closes [#34](https://gitlab.com/ccrpc/workforce-data-portal/issues/34)
* **chart:** Include source url for charts ([447fe81](https://gitlab.com/ccrpc/workforce-data-portal/commit/447fe81199140716ad75f1093c5a4fe90aa1740b))
* **chart:** Initial work to connect filter to chart ([a6f5872](https://gitlab.com/ccrpc/workforce-data-portal/commit/a6f587252850f7b5ee32bde69f382e6ee16029f4))
* **content:** Initial work on implementing error message. ([670fa47](https://gitlab.com/ccrpc/workforce-data-portal/commit/670fa47e43fdbf6898761b3a3ff2e518698866c6))
* **data:** Add aggregated ACS data ([baeedb1](https://gitlab.com/ccrpc/workforce-data-portal/commit/baeedb1abde675b2e0af07009ea41cf5f66fae1e))
* **data:** Add QCEW data and connect to chart. ([87cf2f2](https://gitlab.com/ccrpc/workforce-data-portal/commit/87cf2f268186cbd939cc8511f73280df4bd763d9))
* **data:** Add WARN data source to the portal. ([3b11396](https://gitlab.com/ccrpc/workforce-data-portal/commit/3b1139631d68d64671d5e049bfa191495603a245))
* **data:** Connect LAUS data. ([edc2af6](https://gitlab.com/ccrpc/workforce-data-portal/commit/edc2af698e1ca318ac8ea7cca7526cee97f2fc75))
* **data:** Update workforce data. ([552655d](https://gitlab.com/ccrpc/workforce-data-portal/commit/552655d750d321e6a5c833503f12a583381b7d81))
* **dataSource:** Add label replacement for workforceSource ([e37dcfa](https://gitlab.com/ccrpc/workforce-data-portal/commit/e37dcfaca1c09c1322f2da5122b8c8ac78974ff6))
* **filter:** Add event emitter for filter change. ([7d40cdd](https://gitlab.com/ccrpc/workforce-data-portal/commit/7d40cddcb3deedd153c2311602b398bd618a3a4c))
* **filter:** Add event listener for filter change ([4adc755](https://gitlab.com/ccrpc/workforce-data-portal/commit/4adc755a6cb9d22cbfd157bb76e12961039b6e42))
* **filter:** Connect scenario filter to advanced filter ([a07af90](https://gitlab.com/ccrpc/workforce-data-portal/commit/a07af904d4cf832ab14dceb584c9e1ec546e3268))
* **filters:** Add default selection. ([26de2f7](https://gitlab.com/ccrpc/workforce-data-portal/commit/26de2f7acf33cce209f5083b0c300331abf2850b))
* **info:** Update info popup description ([aaf0140](https://gitlab.com/ccrpc/workforce-data-portal/commit/aaf0140fe803e4282d28929528f854ee1a556ced)), closes [#27](https://gitlab.com/ccrpc/workforce-data-portal/issues/27)
* **share:** Add share button. ([619fe7a](https://gitlab.com/ccrpc/workforce-data-portal/commit/619fe7a3fde8bf7a9c32541f7bf66698653d8f98))


### Bug Fixes

* **advancedButton:** Removed ion-padding for the button ([1708e21](https://gitlab.com/ccrpc/workforce-data-portal/commit/1708e21e321763fa6cd80cab6f609b978dbbbb92)), closes [#23](https://gitlab.com/ccrpc/workforce-data-portal/issues/23)
* **chart:** Add width property to RPC chart ([068aed6](https://gitlab.com/ccrpc/workforce-data-portal/commit/068aed6413f5265062a08a279d8733b71d871c08))
* **chart:** Fix business type source mapping. ([7dad092](https://gitlab.com/ccrpc/workforce-data-portal/commit/7dad092fdca2a11552d9c4709431900643799df7))
* **chart:** Fix compare by filter for business size. ([3fa7dd8](https://gitlab.com/ccrpc/workforce-data-portal/commit/3fa7dd8d4c586bfa82e15c323e3e795e03720e7f)), closes [#33](https://gitlab.com/ccrpc/workforce-data-portal/issues/33)
* **chart:** Fix description for annual average pay chart. ([1b68bf9](https://gitlab.com/ccrpc/workforce-data-portal/commit/1b68bf98a35c2177df3b3ce464184e17e814b1dd)), closes [#30](https://gitlab.com/ccrpc/workforce-data-portal/issues/30)
* **chart:** Fix employment chart allowed compareBy filter ([ce83381](https://gitlab.com/ccrpc/workforce-data-portal/commit/ce833817deea090d784fd22e59fc0be42d74537f))
* **chart:** Fix typo for filtering message. ([94d145b](https://gitlab.com/ccrpc/workforce-data-portal/commit/94d145b1ef56e0a66f2d7dbb377aec4810805d3c)), closes [#28](https://gitlab.com/ccrpc/workforce-data-portal/issues/28)
* **chart:** Fix typo in turnover rate title. ([b57aedf](https://gitlab.com/ccrpc/workforce-data-portal/commit/b57aedf3e29a45bf40d458151040339c98f104bd)), closes [#29](https://gitlab.com/ccrpc/workforce-data-portal/issues/29)
* **data:** Update data mapping. ([a231753](https://gitlab.com/ccrpc/workforce-data-portal/commit/a231753a82f189368f555d2420213da29957eb8a)), closes [#26](https://gitlab.com/ccrpc/workforce-data-portal/issues/26)
* add IE11 CSS hack for dropdown size ([1b454fe](https://gitlab.com/ccrpc/workforce-data-portal/commit/1b454fe483e0a2f0fdda63626398b956a63c482a)), closes [#24](https://gitlab.com/ccrpc/workforce-data-portal/issues/24)
* **chart:** Remove y-min and y-max for turnover chart ([ce738c8](https://gitlab.com/ccrpc/workforce-data-portal/commit/ce738c81b06f0d52cff93ea40bf455f7b0b29fe2))
* **datasource:** Fix sanitized data not cacahe correctly ([a0a286e](https://gitlab.com/ccrpc/workforce-data-portal/commit/a0a286ee2e43c29aae5128de4eb10f353a24c6e0))
* **datasource:** Replace empty string with null for chart ([5506512](https://gitlab.com/ccrpc/workforce-data-portal/commit/5506512af4ebb71cc99c59fb853d7266b3dfdab0))
* **filter:** Add min-width CSS for filter. ([4a68a6a](https://gitlab.com/ccrpc/workforce-data-portal/commit/4a68a6a7be9bf65c8e8614afe60abeac04b23cd2)), closes [#8](https://gitlab.com/ccrpc/workforce-data-portal/issues/8)
* **filter:** Fix all filter options ([bd59915](https://gitlab.com/ccrpc/workforce-data-portal/commit/bd59915247fc4f51c796116c34613e2ad2191f62))
* **regex:** Dynamically construct regex for chart. ([5e981cb](https://gitlab.com/ccrpc/workforce-data-portal/commit/5e981cb49118803189263366e47013f8ce621b25)), closes [#18](https://gitlab.com/ccrpc/workforce-data-portal/issues/18)
